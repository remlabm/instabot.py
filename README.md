## Dockerized - InstaBot

[InstaBot v 1.1.0](https://github.com/instabot-py/instabot.py) 

Docker Source: https://gitlab.com/remlabm/instabot.py
Upstream: https://github.com/instabot-py/instabot.py

## Usage
`docker run -it -e LOGIN=INSTA_LOGIN -e PASSWORD=INSTA_PASSWORD -e 'TAG_LIST=follow4follow f4f' remlabm/instabot.py` 

| Parameter            | Type|                Description                           |        Default value             | 
|:--------------------:|:---:|:----------------------------------------------------:|:--------------------------------:|:
| LOGIN                | str | your instagram username                              |                                  | 
| PASSWORD             | str | your instagram password                              |                                  | 
| LIKE_PER_DAY         | int | how many likes the bot does in 1 day                 | 1000                             | 
| MEDIA_MAX_LIKE       | int | don't like if media has more than ... likes          | 0                                | 
| MEDIA_MIN_LIKE       | int | don't like if media has less than ... likes          | 0                                | 
| FOLLOW_PER_DAY       | int | how many users to follow in 1 day                    | 0                                | 
| FOLLOW_TIME          | int | how many times passes before the  bot unfollows a followed user (sec) | 5 * 60 * 60     | 
| UNFOLLOW_PER_DAY     | int | how many user unfollows the bot does in day          | 0                                | 
| COMMENTS_PER_DAY     | int | how many comments the bot writes in a day            | 0                                | 
| COMMENT_LIST         | str | list of list of words, each of which will be used to generate comment |  |
| TAG_LIST             | str | list of tag the bot uses                             | 'f4f f2f'                        |
| TAG_BLACKLIST        | str | list of tags the bot refuses to like                 | []                               | 
| USER_BLACKLIST       | str | don't like posts from certain users                  | {}                               | 
| MAX_LIKE_FOR_ONE_TAG | int | bot get 21 media from one tag, how many use by row   | 5                                | 
| UNFOLLOW_BREAK_MIN   | int | Minimum seconds for unfollow break pause             | 15                               | 
| UNFOLLOW_BREAK_MAX   | int | Maximum seconds for unfollow break pause             | 30                               | 
| LOG_MOD              | int | logging mod                                          | 0                                | 
| PROXY                | str | Access instagram through a proxy server              |                                  | 
                                                                                                                         
## Warning!
The End-User assumes sole responsibility for anything resulting from the use or modification this program.
Don't worry the bot will not:
- Like - comment or follow your own account or media
- Comment a media already commented

## Community

- [Telegram Group](https://t.me/joinchat/AAAAAEG_8hv3PIJnmq1VVg)
- [Facebook Group](https://www.facebook.com/groups/instabot/)

[1]: http://developers.instagram.com/post/133424514006/instagram-platform-update
[2]: https://www.instagram.com
[3]: https://www.python.org/dev/peps/pep-0008/#source-file-encoding
[4]: https://github.com/LevPasha/Instagram-API-python